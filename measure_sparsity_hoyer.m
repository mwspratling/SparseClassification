function s=measure_sparsity_hoyer(y)
[n,p]=size(y);
sqrtn=sqrt(n);
for i=1:p
  s(i)=(sqrtn-(norm(y(:,i),1)/norm(y(:,i),2)))/(sqrtn-1);
end
s=mean(s);
