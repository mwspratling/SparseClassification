function [y,e,sTrace,yTrace]=dim_activation(W,V,x)
[n,m]=size(W);
[nInputChannels,batchLen]=size(x);

epsilon1=1e-9;
epsilon2=1e-3;
maxIterations=750;

if nargout>2, sTrace=zeros(1,maxIterations); end
if nargout>3, yTrace=zeros(n,maxIterations); end

%set initial prediction node activations
y=zeros(n,batchLen,'single'); 

ediff=Inf;eprev=0;
%iterate to find steady-state response to input 
t=0;
while t<maxIterations && ediff>0.04
  t=t+1;

  e=x./(epsilon2+(V'*y));
  y=(epsilon1+y).*(W*e);

  if nargout>2, sTrace(t)=measure_sparsity_hoyer(y); end
  if nargout>3, yTrace(:,t)=y; end

  ediff=sum(abs(e-eprev));
  eprev=e;
end
%disp(['its=',int2str(t),' ediff=',num2str(ediff)]);
%fprintf(1,'.%i.',t);